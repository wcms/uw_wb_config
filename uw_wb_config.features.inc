<?php

/**
 * @file
 * uw_wb_config.features.inc
 */

/**
 * Implements hook_views_api().
 */
function uw_wb_config_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
