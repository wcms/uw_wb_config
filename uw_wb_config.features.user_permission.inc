<?php

/**
 * @file
 * uw_wb_config.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_wb_config_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'access workbench'.
  $permissions['access workbench'] = array(
    'name' => 'access workbench',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'award content author' => 'award content author',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'emergency alerter' => 'emergency alerter',
      'events editor' => 'events editor',
      'form editor' => 'form editor',
      'form results access' => 'form results access',
      'notices editor' => 'notices editor',
      'site manager' => 'site manager',
      'special alerter' => 'special alerter',
    ),
    'module' => 'workbench',
  );

  // Exported permission: 'administer workbench'.
  $permissions['administer workbench'] = array(
    'name' => 'administer workbench',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'workbench',
  );

  // Exported permission: 'administer workbench moderation'.
  $permissions['administer workbench moderation'] = array(
    'name' => 'administer workbench moderation',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'workbench_moderation',
  );

  // Exported permission: 'bypass workbench moderation'.
  $permissions['bypass workbench moderation'] = array(
    'name' => 'bypass workbench moderation',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'events editor' => 'events editor',
      'site manager' => 'site manager',
    ),
    'module' => 'workbench_moderation',
  );

  // Exported permission: 'file deletion wcms all pages'.
  $permissions['file deletion wcms all pages'] = array(
    'name' => 'file deletion wcms all pages',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'uw_wb_config',
  );

  // Exported permission: 'moderate content from archived to draft'.
  $permissions['moderate content from archived to draft'] = array(
    'name' => 'moderate content from archived to draft',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'events editor' => 'events editor',
      'site manager' => 'site manager',
    ),
    'module' => 'workbench_moderation',
  );

  // Exported permission: 'moderate content from draft to needs_review'.
  $permissions['moderate content from draft to needs_review'] = array(
    'name' => 'moderate content from draft to needs_review',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'award content author' => 'award content author',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'events editor' => 'events editor',
      'site manager' => 'site manager',
    ),
    'module' => 'workbench_moderation',
  );

  // Exported permission: 'moderate content from draft to published'.
  $permissions['moderate content from draft to published'] = array(
    'name' => 'moderate content from draft to published',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'events editor' => 'events editor',
      'site manager' => 'site manager',
    ),
    'module' => 'workbench_moderation',
  );

  // Exported permission: 'moderate content from needs_review to archived'.
  $permissions['moderate content from needs_review to archived'] = array(
    'name' => 'moderate content from needs_review to archived',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'events editor' => 'events editor',
      'site manager' => 'site manager',
    ),
    'module' => 'workbench_moderation',
  );

  // Exported permission: 'moderate content from needs_review to draft'.
  $permissions['moderate content from needs_review to draft'] = array(
    'name' => 'moderate content from needs_review to draft',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'events editor' => 'events editor',
      'site manager' => 'site manager',
    ),
    'module' => 'workbench_moderation',
  );

  // Exported permission: 'moderate content from needs_review to published'.
  $permissions['moderate content from needs_review to published'] = array(
    'name' => 'moderate content from needs_review to published',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'events editor' => 'events editor',
      'site manager' => 'site manager',
    ),
    'module' => 'workbench_moderation',
  );

  // Exported permission: 'moderate content from published to archived'.
  $permissions['moderate content from published to archived'] = array(
    'name' => 'moderate content from published to archived',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'workbench_moderation',
  );

  // Exported permission: 'use workbench_moderation archive content tab'.
  $permissions['use workbench_moderation archive content tab'] = array(
    'name' => 'use workbench_moderation archive content tab',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'events editor' => 'events editor',
      'site manager' => 'site manager',
    ),
    'module' => 'uw_wb_config',
  );

  // Exported permission: 'use workbench_moderation my drafts tab'.
  $permissions['use workbench_moderation my drafts tab'] = array(
    'name' => 'use workbench_moderation my drafts tab',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'award content author' => 'award content author',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'events editor' => 'events editor',
      'site manager' => 'site manager',
    ),
    'module' => 'workbench_moderation',
  );

  // Exported permission: 'use workbench_moderation needs review tab'.
  $permissions['use workbench_moderation needs review tab'] = array(
    'name' => 'use workbench_moderation needs review tab',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'award content author' => 'award content author',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'events editor' => 'events editor',
      'site manager' => 'site manager',
    ),
    'module' => 'workbench_moderation',
  );

  // Exported permission: 'view all unpublished content'.
  $permissions['view all unpublished content'] = array(
    'name' => 'view all unpublished content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'emergency alerter' => 'emergency alerter',
      'events editor' => 'events editor',
      'site manager' => 'site manager',
      'special alerter' => 'special alerter',
    ),
    'module' => 'workbench_moderation',
  );

  // Exported permission: 'view moderation history'.
  $permissions['view moderation history'] = array(
    'name' => 'view moderation history',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'award content author' => 'award content author',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'events editor' => 'events editor',
      'site manager' => 'site manager',
    ),
    'module' => 'workbench_moderation',
  );

  // Exported permission: 'view moderation messages'.
  $permissions['view moderation messages'] = array(
    'name' => 'view moderation messages',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'award content author' => 'award content author',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'events editor' => 'events editor',
      'site manager' => 'site manager',
    ),
    'module' => 'workbench_moderation',
  );

  return $permissions;
}
